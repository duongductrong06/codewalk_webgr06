<?php

const BRANDING = [
    1 => "XPS",
    2 => "Inspiron",
    3 => "Latitude",
    4 => "Vostro",
    5 => "Alienware",
];

const PRICING = [
    1 => "Trên 50 triệu",
    2 => "40 - 50 triệu",
    3 => "30 - 40 triệu",
    4 => "20 - 30 triệu",
    5 => "15 - 20 triệu",
    6 => "10 - 15 triệu",
    7 => "Dưới 10 triệu",
];

const CATEGORIZING = [
    1 => "Nhập khẩu",
    2 => "Chính hãng",
];

const STATUTING = [
    1 => "New Sealed",
    2 => "New, Fullbox",
    3 => "New, Outlet",
    4 => "Used",
];

const CPU = [
    1 => "Intel i5",
    2 => "Intel i7",
    3 => "Intel i3",
    4 => "AMD Ryzen 3",
    5 => "AMD Ryzen 5",
    6 => "AMD Ryzen 7",
];

const RAM = [
    1 => "4GB",
    2 => "8GB",
    3 => "16GB",
    4 => "32GB",
    5 => "64GB",
    6 => "128GB",
];

const HARDWARE = [
    1 => "SSD",
    2 => "HDD"
];

const SCREEN = [
    1 => "13 inch",
    2 => "15 inch",
    3 => "16 inch"
];

const FREQUENCY = [
    1 => "59.99hz",
    2 => "60hz",
    3 => "120hz"
];

const WEIGHT = [
    1 => "Dưới 1KG",
    2 => "1-3KG",
    3 => "Trên 3KG"
];

const HOST = "/";

function str_replace_last( $search , $replace , $str ) {
    if( ( $pos = strrpos( $str , $search ) ) !== false ) {
        $search_length  = strlen( $search );
        $str    = substr_replace( $str , $replace , $pos , $search_length );
    }
    return $str;
}