<?php

require_once("app/classes/Route.php");

use App\Classes\Route;

?>
  <!--HEADER-->
  <div class="header">
    <!--HEADER--TOP-->
    <!--HEADER-->
    <!--HEADER--TOP-->
    <div class="strip justify-content-between align-items-center px-4  d-none d-md-flex">
      <p class="font-rale font-size-12 text-black-50 mt-1 mb-0 ">Welcome to Nhóm 6</p>
      <div class="font-rale font-size-14">
        <a href="#" class="px-3 border-right border-left text-black-50"><i class="fas fa-map-marker-alt"></i> 279 Nguyễn Tri Phương</a>
        <a href="#" class="px-3 border-right text-black-50"><i class="fas fa-phone-alt"></i> 0123456789</a>
        <a href="#" class="px-3 border-right text-black-50"><i class="fab fa-facebook-f"></i></a>
        <a href="#" class="px-3 border-right text-black-50"><i class="fab fa-instagram "></i></a>
      </div>
    </div>


    <!--header navigation-->
    <nav class="navbar navbar-expand-md navbar-light" style="margin:0px;padding: 0px;">
      <a class="navbar-brand" href="#"><img class="d-none d-md-flex" style="width: 205px;height: 60px;" src="public/img/logo/logo1.png"></a>
      <div class="container">

        <!--search khi màn hình nhỏ-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#"><img class=" img-fluid d-flex d-md-none" style="width: 120px;height: 60px;" src="public/img/logo/logo1.png"></a>
        <div class="header-wrap-icon  d-flex d-md-none">
          <form class="col-sm-8" id="searchbox" action="<?= Route::getCurrentHost() ?>" method="GET">
            <input type="search" name="q" value="<?= Route::getQuery("q") ?>" placeholder="Search">
            <?php
              foreach($_GET as $name => $value) {
                if($name !== "q") {
                  $name = htmlspecialchars($name);
                  $value = htmlspecialchars($value);
                  echo '<input type="hidden" name="'. $name .'" value="'. $value .'">';
                }
              }
              ?>
          </form>
        </div>

        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item mx-3">
              <div class="dropdown show">
                <a class="btn " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span style="font-size: 80%"><i class="fas1 fas fa-laptop"></i></span>
                  <span style="font-size: 120%">Laptop</span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Apple</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Macbook Pro 13</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Macbook Pro 15</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Macbook Pro 16</button>

                    </div>
                  </div>
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Dell</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Inspiron</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Vostro</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">G-Gaming Series</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Latitude</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Precision</button>
                    </div>
                  </div>
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">HP</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Elitebook</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">ZBook</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Envy</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Precision</button>
                    </div>
                  </div>
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Asus</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">ExpertBook</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">ZBook</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Vivobook</button>
                    </div>
                  </div>
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black; border: 0px;" data-toggle="dropdown">Lenovo</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">ThinkPad</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">IdeaPad</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">ThinkBook</button>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="nav-item mx-3">
              <div class="dropdown show">
                <a class="btn " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span style="font-size: 80%"><i class="fas1 fas fa-desktop"></i></span>
                  <span style="font-size: 120%">Máy tính - PC</span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Apple</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Imac</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Mac</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Mac Pro</button>
                    </div>
                  </div>
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Dell</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Optiplex</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Precision5</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Alienware</button>
                    </div>
                  </div>
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">HP</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Elitedesk</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Workstation</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Pavilion</button>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="nav-item mx-3">
              <div class="dropdown show">
                <a class="btn " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span style="font-size: 80%"><i class="fas1 fas fa-headphones"></i></span>
                  <span style="font-size: 120%">Phụ kiện</span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Phụ kiện máy tính</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Bàn phím & chuột</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Ba lô, túi chống sốc</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Tấm lót chuột</button>
                    </div>
                  </div>
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Thiết bị lưu trữ</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">USB</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Ổ cúng di động</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Thẻ nhớ</button>
                    </div>
                  </div>
                  <div class="dropright">
                    <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Thiết bị âm thanh</button>
                    <div class="dropdown-menu">
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Loa</button>
                      <button class="btn btn-outline-warning col-12" style="color:black;border: 0px;" data-toggle="dropdown">Tai nghe</button>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="nav-item button mx-3">
              <div class="dropdown show">
                <a class="btn " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span style="font-size: 80%"><i class="fas1 fas fa-newspaper"></i></span>
                  <span style="font-size: 120%">Trang tin</span>
                </a>
              </div>
            </li>
          </ul>
          <hr>
          <!--đường gạch ngang-->
          <!--search khi màn hình >md-->
          <ul class="navbar-nav">
            <div class="header-wrap-icon d-none d-md-flex">
              <form class="col-sm-8" id="searchbox" action="<?= Route::getCurrentHost() ?>">
                <input type="search" name="q" value="<?= Route::getQuery("q") ?>" placeholder="Search">

                <?php
                  foreach($_GET as $name => $value) {
                    if($name !== "q") {
                      $name = htmlspecialchars($name);
                      $value = htmlspecialchars($value);
                      echo '<input type="hidden" name="'. $name .'" value="'. $value .'">';
                    }
                  }
                  ?>
              </form>
            </div>
          </ul>

          <ul class="navbar-nav d-none d-md-flex">
            <li class="nav-item mx-3">
              <a class="nav-link" href="#"><i class="fa fa-bell" aria-hidden="true"></i></a>
            </li>
            <li class="nav-item mx-3">
              <a class="nav-link" href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
            </li>
            <li class="nav-item mx-3">
              <a class="nav-link" href="#"><i class="fas fa-user"></i></a>
            </li>
          </ul>
          <!--hiện trong button khi màn hình nhỏ-->
          <ul class="navbar-nav d-flex d-md-none">
            <li class="nav-item">
              <div class="col-12 text-center">
                <i class="fab fa-facebook mr-3"></i>
                <i class="fab fa-instagram "></i>
              </div>
              <div class="col-12 text-center">
                <i class="fa fa-phone" aria-hidden="true"></i> 0123456789
              </div>
              <div class="col-12 text-center">
                <i class="fas fa-map-marker-alt"></i> 279 Nguyễn Tri Phương
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>


  <div class="container">
    <div class="container-fluid breadcrumb1" style="padding: 0px">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#" style=" color: #cc5801">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Dell</li>
        </ol>
      </nav>
    </div>
  </div>