<?php

use App\Classes\Route;

include "app/views/_common/head.php";
?>


<?php
include "app/views/_common/header.php";
?>

<div class="container menu-tab" style="margin-top: 10px">
    <div class="row">
        <div class="col-md-8 col-12" style="padding-right:0px; display:inline-block;">
            <div class="row">
                <div class="col-3">
                    <p style="font-weight: bold;">Kết quả tìm kiếm cho:</p>
                </div>
                <div class="col-9">
                    <p><?= Route::getQuery("q") ? "\"" . Route::getQuery("q") . "\"" : "Bạn chưa tìm kiếm" ?></p>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-12" style="padding-right:0px;">
            <div class="row">
                <div class="col-7 sapxeptheo">
                    <p style="margin-bottom:2px; padding-left: 10px;">Sắp xếp theo:</p>
                </div>
                <div class="col-5" style="padding-left:35px">
                    <div>
                        <select name="calc_shipping_provinces" style="text-align: right;">
                            <option <?= Route::getQuery("sort") === "asc" ? "selected" : "" ?> value="asc"> Giá tăng dần</option>
                            <option <?= Route::getQuery("sort") === "desc" ? "selected" : "" ?> value="desc"> Giá giảm dần</option>
                        </select>

                        <script>
                            $("select[name='calc_shipping_provinces']").change(function() {
                                const sortKey = $(this).val();

                                const oldUrl = "<?= Route::appends("sort", "_REPLACE_KEY_") ?>";

                                window.location.href = oldUrl.replace("_REPLACE_KEY_", sortKey);
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container imgsp body">
    <div class="row">
        <?php include("app/views/_common/filter.php") ?>

        <div class="col-md-9 p-0 ">
            <div class="container-fluid tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class="row">
                        <?php if (count($data['products']) !== 0) { ?>
                            <?php foreach ($data['products'] as $product) { ?>
                                <div class="col-md-4 col-6 ">
                                    <div class="tovar_wrapper p-0" data-appear-top-offset='-100' data-animated='fadeInUp'>
                                        <div class="card  tovar_item padbot40 filter ">
                                            <div class="tovar_img">
                                                <div class="tovar_img_wrapper">
                                                    <label class="baohanh badge-warning" style="color: black; margin:10px; font-size: 12px;">Trả góp 0%</label>
                                                    <img class="card-img-top" style="object-fit: cover;" src="public/<?= $product->hinhanh_url ?>" alt="">
                                                    <div class="card-body">
                                                        <p class="card-title text-center" style="font-weight: bold;"><?= $product->tensanpham ?></p>
                                                        <p class="special-price card-text text-danger my-2" style="font-weight: bold;display: inline;"><?= $product->giagiam ?> ₫</p>
                                                        <p class="old-price" style="text-decoration: line-through;display: inline; font-size: 13px; color: gray;"><?= $product->giaban ?> ₫</p>
                                                        <br>
                                                        <div class="rating text-warning my-2">
                                                            <span>
                                                                <?php for ($i = 0; $i < (int)$product->diem; $i++) { ?>
                                                                    <i class="fas fa-star"></i>
                                                                <?php } ?>
                                                                <?php for ($i = (int)$product->diem; $i < 5; $i++) { ?>
                                                                    <i class="far fa-star"></i>
                                                                <?php } ?>
                                                                <span style="color: black"><?= (int) $product->danhgia ?> đánh giá</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tovar_item_btns">
                                                    <div class="open-project-link"><a class="open-project tovar_view" href="?controller=DetailPage&action=index&idProduct='<?= $product->id_sanpham ?>">Chi tiết</a></div>
                                                    <a class="add_bag" href="GioHang.html"><i class="fa fa-shopping-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <h2 class="col-12 my-5 text-center h5">Không có dữ liệu</h2>
                        <?php } ?>
                    </div>
                </div>



                <div class="container pagination">
                    <?php
                    $currentPage = $data["page"];
                    $totalPages = ceil($data["total"] / $data["perPage"]);
                    $next = $currentPage < $totalPages ? ($currentPage + 1) : ($currentPage);
                    $prev = $currentPage > 1 ? ($currentPage - 1) : 1;

                    $step1 = $currentPage < $totalPages;
                    $step2 = 1;
                    $step3 = 1;
                    ?>

                    <?php if ($totalPages > 1) { ?>
                        <ul class="pagination">
                            <?php if ($currentPage > $prev) { ?>
                                <li class="page-item"><a class="page-link" href="<?= Route::appends("page", $prev) ?>">Previous</a></li>
                            <?php } ?>

                            <!-- First number -->
                            <?php if ($currentPage === 1 || $currentPage === 0) { ?>
                                <li class="page-item active"><a class="page-link" href="<?= Route::appends("page", 1) ?>">1</a></li>
                            <?php } ?>

                            <!-- Step number -->
                            <?php if ($currentPage > 1) { ?>
                                <li class="page-item"><a class="page-link" href="<?= Route::appends("page", $currentPage - 1) ?>"><?= $currentPage - 1 ?></a></li>
                            <?php } ?>

                            <!-- Step number -->
                            <?php if ($currentPage > 1 && $currentPage < $totalPages) { ?>
                                <li class="page-item active"><a class="page-link" href="<?= Route::appends("page", $currentPage) ?>"><?= $currentPage ?></a></li>
                            <?php } ?>

                            <!-- Step number -->
                            <?php if ($currentPage + 1 > 1 && $currentPage + 1 < $totalPages) { ?>
                                <li class="page-item "><a class="page-link" href="<?= Route::appends("page", $currentPage + 1) ?>"><?= $currentPage + 1 ?></a></li>
                            <?php } ?>

                            <!-- Step number -->
                            <?php if ($currentPage + 1 === (int) $totalPages) { ?>
                                <li class="page-item "><a class="page-link" href="<?= Route::appends("page", $currentPage + 1) ?>"><?= $currentPage + 1 ?></a></li>
                            <?php } ?>

                            <!-- Last Step number -->
                            <?php if ($currentPage === (int) $totalPages) { ?>
                                <li class="page-item active"><a class="page-link" href="<?= Route::appends("page", $currentPage) ?>"><?= $currentPage ?></a></li>
                            <?php } ?>

                            <?php if ($currentPage < $next) { ?>
                                <li class="page-item"><a class="page-link" href="<?= Route::appends("page", $next) ?>">Next</a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include "app/views/_common/footer.php"; ?>