<?php

namespace App\Classes;

use PDO;

class DB
{
    protected static $table;
    private static $instance = null;
    private static $where = "";

    function __construct()
    {
        // global $DB_NAME, $DB_USER, $DB_PASSWORD;
        // $this->db = new PDO("mysql:host=125.234.104.133;dbname=$DB_NAME", $DB_USER, $DB_PASSWORD);
        // $this->db->exec("SET CHARACTER SET utf8");
    }

    public static function pdo_driver()
    {
        global $DB_NAME, $DB_USER, $DB_PASSWORD;
        return new PDO("mysql:host=125.234.104.133;dbname=$DB_NAME", $DB_USER, $DB_PASSWORD);
    }

    public static function query($query)
    {
        $q = self::pdo_driver()->prepare($query);
        $q->execute();

        $res = $q->fetchAll(PDO::FETCH_CLASS);

        return $res;
    }

    public static function table($table)
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        self::$table = $table;

        return self::$instance;
    }

    public function select($column = "*")
    {
        $q = self::pdo_driver()->prepare("select $column from " . self::$table);

        $q->execute();

        $res = $q->fetchAll(PDO::FETCH_CLASS);

        return $res;
    }

    public function count($where = "")
    {
        $q = self::pdo_driver()->prepare("SELECT COUNT(*) FROM " . self::$table . " " . $where);

        $q->execute();

        $res = $q->fetchColumn();

        return $res;
    }

    public static function where($column = "", $operator = "=", $value = "")
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        preg_match('/(\'\%)[\w\d].*(\%\')/', $value, $matched);

        if (strtolower($operator) === "like" && count($matched) !== 0) {
            self::$where .= " WHERE " . $column . " " . $operator . " " . $value;
        } else if (strtolower($operator) !== "like" && !empty($value)) {
            preg_match("/(WHERE)/", self::$where, $hasWhere);

            if (!$hasWhere) {
                self::$where .= " WHERE " . $column . " " . $operator . " " . $value;
            } else {
                self::$where .= " AND " . $column . " " . $operator . " " . $value;
            }
        }

        return self::$instance;
    }

    public function insert()
    {
    }

    public static function update($table = "", $where = "", $data = [])
    {
        try {
            $strSet = "";
            foreach ($data as $key => $value) {
                $strSet .= "$key=$value, ";
            }
            
            $strSet = str_replace_last(", ", "", $strSet);

            $query = "UPDATE $table SET $strSet WHERE $where";

            self::pdo_driver()->prepare($query)->execute();
        } catch (\Exception $e) {
            echo $e;
        }
    }

    public function delete()
    {
    }

    public function join()
    {
    }
}
