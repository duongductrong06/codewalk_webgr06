<?php

namespace App\Classes;

use Exception;

class Route
{

    public static function getCurrentHost()
    {
        return HOST;
    }

    public static function getCurrentRoute()
    {
        return @$_SERVER["PATH_INFO"/*"REQUEST_URI"*/] ?? "/";
    }

    public static function getQuery($query = null, $default = null)
    {
        try {
            if (isset($query)) {
                return $_GET[$query] ?? $default ?? null;
            }

            return $_GET ?? $default;
        } catch (Exception $e) {
            return $_GET ?? $default;
        }
    }

    public static function appends($query, $value)
    {
        $temp = [];
        $temp[$query] = $value;

        $queries = http_build_query(array_merge(self::getQuery(), $temp)); // page=1&test=2 or null
        return "?".$queries;

        // if (empty($queries)) {
        //     return "?" . $query . "=" . $value;
        // }

        // if (strpos($queries, $query)) {
        //     $old = $query . "=" . Route::getQuery($query);
        //     $new = $query . "=" . $value;

        //     return str_replace($old, $new, $queries);
        // } else {
        // }
    }
}
