<?php

namespace App\Classes;


class ViewBase
{
    public static function render($view, $data = [])
    {
        $v = explode("_", $view);

        if (count($v) > 1) {
            echo "Mục này không được phép truy cập vì có chứa (_)";
            return;
        }

        $pathView = "app/views/$view.php";

        require($pathView);
    }
}
