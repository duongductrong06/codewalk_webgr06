<?php

namespace App\Controllers;

class ErrorController extends BaseController {
    public function index()
    {
        echo $this->view->render("404");
    }
}