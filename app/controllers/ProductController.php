<?php

namespace App\Controllers;

require_once("app/controllers/BaseController.php");
require_once("app/models/Product.php");
require_once("app/classes/Route.php");
require_once("app/classes/DB.php");

use App\Classes\DB;
use App\Classes\Route;
use App\Models\Product;

class ProductController extends BaseController
{
    public function index()
    {
        $page = (int) Route::getQuery("page"); // page
        $q = Route::getQuery("q"); // query searching keyword
        $sort = Route::getQuery("sort", "asc");

        if ($page === 0) {
            $page = 1;
        }

        $perPage = 12;
        $query = Product::where("tbl_sanpham.tensanpham", "like", "'%" . $q . "%'")
        ->where("tbl_sanpham.branding_filter", "=", (int) Route::getQuery("branding_filter"))
        ->where("tbl_sanpham.pricing_filter", "=", (int) Route::getQuery("pricing_filter"))
        ->where("tbl_sanpham.category_filter", "=",  (int) Route::getQuery("category_filter"))
        ->where("tbl_sanpham.status_filter", "=", (int) Route::getQuery("status_filter"))
        ->where("tbl_sanpham.cpu_filter", "=", (int) Route::getQuery("cpu_filter"))
        ->where("tbl_sanpham.ram_filter", "=", (int) Route::getQuery("ram_filter"))
        ->where("tbl_sanpham.hdd_filter", "=", (int) Route::getQuery("hdd_filter"))
        ->where("tbl_sanpham.screen_filter", "=", (int) Route::getQuery("screen_filter"))
        ->where("tbl_sanpham.frequency_filter", "=", (int) Route::getQuery("frequency_filter"))
        ->where("tbl_sanpham.weight_filter", "=", (int) Route::getQuery("weight_filter"))
        ->orderBy("tbl_sanpham.giagiam", $sort ?? "asc");

        $products = $query 
            ->paginate($page, $perPage);

        $counts = $query->count();

        $this->view->render("index", [
            "products" => $products,
            "total" => $counts,
            "perPage" => $perPage,
            "page" => $page
        ]);
    }

    public function test()
    {
        $products = Product::paginate(0, 12);

        echo json_encode($products);
    }
}

// Seeding data
        // for ($i=22; $i < 55; $i++) { 
        //     DB::update("tbl_sanpham", "id_sanpham='SP0$i'", [
        //         "branding_filter" => rand(1, 3),
        //         "pricing_filter" => rand(1, 3),
        //         "category_filter" => rand(1, 3),
        //         "status_filter" => rand(1, 3),
        //         "cpu_filter" => rand(1, 3),
        //         "ram_filter" => rand(1, 3),
        //         "hdd_filter" => rand(1, 3),
        //         "screen_filter" => rand(1, 3),
        //         "frequency_filter" => rand(1, 3),
        //         "weight_filter" => rand(1, 3),
        //     ]);
        // }