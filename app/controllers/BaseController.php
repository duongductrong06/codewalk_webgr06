<?php

namespace App\Controllers;

require_once("app/classes/ViewBase.php");

use App\Classes\ViewBase;

abstract class BaseController
{
    protected $view;

    public function __construct()
    {
        $this->view = new ViewBase();
    }

    public function index()
    {
    }
    public function create()
    {
    }
    public function save()
    {
    }
    public function edit()
    {
    }
    public function update()
    {
    }
    public function delete()
    {
    }
}
