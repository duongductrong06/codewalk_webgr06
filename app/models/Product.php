<?php

namespace App\Models;

require("app/classes/DB.php");

use App\Classes\DB;

class Product
{
    private static $table = "tbl_sanpham";

    private static $where = "";

    private static $orderBy = " ORDER BY tbl_sanpham.id_sanpham ASC ";

    private static $instance = null;

    public static function all()
    {
        return DB::query(
            "SELECT * FROM " . self::$table .
                " LEFT JOIN tbl_hinhanh ON " . self::$table . ".id_sanpham=tbl_hinhanh.id_sanpham " .
                "LEFT JOIN tbl_danhgia ON " . self::$table . ".id_sanpham=tbl_danhgia.id_sanpham"
        );
    }

    public static function where($column = "", $operator = "=", $value = "")
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        preg_match('/(\'\%)[\w\d].*(\%\')/', $value, $matched);

        if (strtolower($operator) === "like" && count($matched) !== 0) {
            self::$where .= " WHERE " . $column . " " . $operator . " " . $value;
        } else if (strtolower($operator) !== "like" && !empty($value)) {
            preg_match("/(WHERE)/", self::$where, $hasWhere);

            if (!$hasWhere) {
                self::$where .= " WHERE " . $column . " " . $operator . " " . $value;
            } else {
                self::$where .= " AND " . $column . " " . $operator . " " . $value;
            }
        }

        return self::$instance;
    }

    public static function paginate($page = 1, $limit = 12)
    {
        try {
            if ($page === 0) {
                $page = 1;
            }

            $paginate = " LIMIT $limit OFFSET " . (($page - 1) * $limit);

            $select = 'SELECT  
                tbl_sanpham.*,
                tbl_hinhanh.hinhanh_url,
                COUNT(tbl_danhgia.id_sanpham) as danhgia,
                MAX(tbl_danhgia.diem) as diem
    
                
                FROM tbl_sanpham 
                    LEFT JOIN tbl_hinhanh ON tbl_sanpham.id_sanpham = tbl_hinhanh.id_sanpham AND tbl_hinhanh.hinh_anh_chinh = 1
                    LEFT JOIN tbl_danhgia ON tbl_sanpham.id_sanpham = tbl_danhgia.id_sanpham

                ' . self::$where . '
                
                GROUP BY tbl_sanpham.id_sanpham, tbl_danhgia.id_sanpham ' . self::$orderBy;

            $res = DB::query($select . $paginate);

            return $res;
        } catch (\Exception $e) {
            echo $e;
        }
    }

    public static function orderBy($column, $order)
    {
        self::$orderBy = "ORDER BY $column $order";

        return self::$instance;
    }

    public static function count()
    {
        return DB::table("tbl_sanpham")->count(self::$where);
    }
}
