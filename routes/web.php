<?php

namespace Routes;

require_once("routes/MethodBase.php");

use Routes\MethodBase as RouteWeb;

RouteWeb::get("/", "ProductController::index");
RouteWeb::get("/product", "ProductController::index");


// handle render 404 when url not match with any route on above
if(!RouteWeb::$renderView) {
    $class = "App\Controllers\ErrorController";
    $controller = new $class;

    return $controller->index();

}