<?php

namespace Routes;

require_once("app/classes/Route.php");
require_once("app/controllers/ProductController.php");
require_once("app/controllers/ErrorController.php");

use App\Classes\Route;

class MethodBase
{
    public static $renderView = false;
    public function __construct()
    {
    }

    public static function get($pathname, $controller)
    {
        if ($pathname === Route::getCurrentRoute()) {

            $class = "App\Controllers\\";

            $temporary = explode("::", $controller);

            $controller = $class . $temporary[0];
            $controller = new $controller;

            $method = $temporary[1];

            self::$renderView = true;

            return $controller->{$method}();
        }

        if(!self::$renderView) {
            self::$renderView = false;
        }
    }
}
